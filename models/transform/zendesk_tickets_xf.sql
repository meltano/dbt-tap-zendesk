with tickets as (

    select * from {{ref('zendesk_tickets')}}

), 

metrics as (

    select * from {{ref('zendesk_ticket_metrics')}}

),

organizations as (

    select * from {{ref('zendesk_organizations')}}

),

users as (
    
    select * from {{ ref('zendesk_users') }}

),

final as (

    select
        tickets.*,

        metrics.initially_assigned_at,
        metrics.solved_at,
        metrics.reopens,
        metrics.replies,
        metrics.first_resolution_time_in_minutes_business,
        metrics.first_resolution_time_in_minutes_calendar,
        metrics.full_resolution_time_in_minutes_business,
        metrics.full_resolution_time_in_minutes_calendar,
        metrics.on_hold_time_in_minutes_business,
        metrics.on_hold_time_in_minutes_calendar,
        metrics.reply_time_in_minutes_business,
        metrics.reply_time_in_minutes_calendar,
        metrics.requester_wait_time_in_minutes_business,
        metrics.requester_wait_time_in_minutes_calendar,


        (tickets.status = 'solved' or tickets.status = 'closed') as ticket_solved,

        case
            when (tickets.status = 'solved' or tickets.status = 'closed')
                then 'Solved'
            else 'Unsolved'
        end as ticket_solved_status,

        case
            when (tickets.status = 'solved' or tickets.status = 'closed')
                then 1
            else 0
        end as ticket_solved_counter,

        case
            when (tickets.status = 'solved' or tickets.status = 'closed')
                then 0
            else 1
        end as ticket_unsolved_counter,

        (tickets.status = 'solved' or tickets.status = 'closed') and 
        (metrics.replies is null or metrics.replies = 1) as ticket_one_touch,

        case
            when (tickets.status = 'solved' or tickets.status = 'closed') and 
                 (metrics.replies is null or metrics.replies = 1)
                then 1
            else 0
        end as ticket_one_touch_counter,

        (metrics.reopens > 0) as ticket_reopened,

        case
            when (metrics.reopens > 0)
                then 1
            else 0
        end as ticket_reopened_counter,

        organizations.name as organization_name,
        requesters.name as requester_name,
        requesters.email as requester_email,
        assignees.name as assignee_name,
        assignees.email as assignee_email
    from tickets
    left join metrics 
        on metrics.ticket_id = tickets.ticket_id
    left join organizations 
        on organizations.organization_id = tickets.organization_id
    left join users as requesters
        on requesters.user_id = tickets.requester_id
    left join users as assignees
        on assignees.user_id = tickets.assignee_id
        
)

select * from final
