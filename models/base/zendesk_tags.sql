with source as (
    
    select * from {{var('schema')}}.tags
    
),

renamed as (
    
    select 
    
        -- The name is also the id
        name as tag_name,

        -- Total tag count
        count as tag_count
        
    from source
    
)

select * from renamed