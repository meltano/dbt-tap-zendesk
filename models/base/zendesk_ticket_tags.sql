with source as (
    
    select * from {{var('schema')}}.tickets
    
), 

renamed as (

    select

        -- This table just connects the Tickets with the tags
        id as ticket_id, 

        jsonb_array_elements_text(tags) as tag_name

    from source
    
)

select * 

from renamed

where tag_name is not null
